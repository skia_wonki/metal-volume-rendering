#include <metal_stdlib>
using namespace metal;
#include <SceneKit/scn_metal>

bool equal(float a, float b) {
    return fabs(a - b) < 0.0001;
}

bool equal(float3 a, float3 b) {
    return equal(a.x, b.x)
    && equal(a.y, b.y)
    && equal(a.z, b.z);
}

struct VertexInput
{
    float3 position [[ attribute(SCNVertexSemanticPosition) ]];
    float2 uv [[attribute(SCNVertexSemanticTexcoord0)]];
};

struct NodeBuffer {
    float4x4 modelTransform;
    float4x4 modelViewTransform;
    float4x4 normalTransform;
    float4x4 modelViewProjectionTransform;
    float2x3 boundingBox;
};

struct VertexOutput
{
    float4 position [[position]];
    float2 pixelCoord;
    float3 color;
};

vertex VertexOutput backface_vertex(
    VertexInput in [[ stage_in ]]
    , constant SCNSceneBuffer& scn_frame [[buffer(0)]]
    , constant NodeBuffer& scn_node [[buffer(1)]])
{
    VertexOutput out;
    out.position = scn_node.modelViewProjectionTransform
        * float4(in.position, 1.0);
    out.color = out.position.xyz;
    return out;
}

fragment float4 backface_fragment(VertexOutput in [[stage_in]])
{
    float4 color = float4(in.color, 1.0);
    return color;
}

struct MipUniforms
{
    float2 viewport;
};

sampler backfacesample
    = sampler(coord::normalized,
              mag_filter::linear,
              min_filter::linear,
              s_address::clamp_to_edge,
              t_address::clamp_to_edge);

sampler volumesample
    = sampler(coord::normalized,
              mag_filter::nearest,
              min_filter::nearest,
              s_address::clamp_to_edge,
              t_address::clamp_to_edge,
              r_address::clamp_to_edge);

vertex VertexOutput mip_vertex
(
    VertexInput in [[ stage_in ]]
    , constant SCNSceneBuffer& scn_frame [[buffer(0)]]
    , constant NodeBuffer& scn_node [[buffer(1)]]
    , constant MipUniforms& uniforms [[buffer(2)]]
)
{
    VertexOutput out;
    out.position = scn_node.modelViewProjectionTransform
        * float4(in.position, 1.0);
    out.color = out.position.xyz;
    
    float2 viewport = uniforms.viewport;
    out.pixelCoord = (in.position.xy - viewport * 0.5) / viewport;
    return out;
}

fragment float4 mip_fragment
(
    VertexOutput in [[stage_in]]
    , texture2d<float, access::sample> backface [[texture(0)]]
    , texture3d<float, access::sample> volume [[texture(1)]]
)
{
    const float STEP_SIZE = 0.005;
    const int MAX_ITERATIONS = 300;
    
    float3 entryPoint = in.color;
    float3 exitPoint = backface.sample(backfacesample, in.pixelCoord).xyz;
    
    if (equal(entryPoint, exitPoint))
        discard_fragment();
    
    float3 dir = exitPoint - entryPoint;
    float len = length(dir);
    float3 dirN = normalize(dir);
    float3 deltaDir = dirN * STEP_SIZE;
    float deltaDirLen = length(deltaDir);
    
    float3 voxelCoord = entryPoint + deltaDir;
    
    float lengthAcum = 0;
    float maxIntensity = 0;
    
    if (is_null_texture(volume))
        return float4(1, 0, 0, 1);
    
    for(int i = 0; i < MAX_ITERATIONS; i++) {
        float intensity = volume.sample(volumesample, voxelCoord).r;
        if (intensity > maxIntensity)
            maxIntensity = intensity;
            
        voxelCoord += deltaDir;
        lengthAcum += deltaDirLen;
        if(lengthAcum >= len) break;
    }
    
    return float4(maxIntensity);
}




