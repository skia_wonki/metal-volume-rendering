//
//  ViewController.swift
//  Metal-Volume-Rendering
//
//  Created by wonki on 2021/08/29.
//

import MetalKit
import SceneKit

class MetalTesterVC: UIViewController {
    @IBOutlet var scnView: SCNView!
    
    var technique: SCNTechnique?
    var cubeNode = SCNNode()
    var texture: MTLTexture?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setScnview()
        // set 3d texture at runtime
        // read multiple image to set 3d texture
    }
    
    func setScnview() {
        scnView.scene = SCNScene()
        let scene = scnView.scene!
        let root = scene.rootNode
        
        scnView.autoenablesDefaultLighting = true
        scnView.showsStatistics = true
        
        cubeNode.geometry = SCNBox(width: 0.5,
                                   height: 0.5,
                                   length: 0.5,
                                   chamferRadius: 0.0)
        cubeNode.categoryBitMask = 2
        root.addChildNode(cubeNode)
        
        let torusNode = SCNNode(geometry: SCNTorus(ringRadius: 0.2, pipeRadius: 0.1))
        torusNode.position = SCNVector3Make(0.7, 0, 0)
        torusNode.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        torusNode.geometry?.firstMaterial?.transparency = 0.25
        torusNode.geometry?.firstMaterial?.transparencyMode = .singleLayer
        root.addChildNode(torusNode)

        let torusNode2 = SCNNode(geometry: SCNTorus(ringRadius: 0.2, pipeRadius: 0.1))
        torusNode2.position = SCNVector3Make(-0.7, 0, 0)
        torusNode2.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
        torusNode2.geometry?.firstMaterial?.transparency = 0.25
        root.addChildNode(torusNode2)
        
        if let path = Bundle.main.path(
            forResource: "volumerendering-metal",
            ofType: "plist")
        {
            if let dico1 = NSDictionary(contentsOfFile: path) {
                let dico = dico1 as! [String: AnyObject]
                let technique = SCNTechnique(dictionary: dico)
                
                let screenSize = NSValue(
                    cgSize: view.frame.size.applying(CGAffineTransform(scaleX: 2.0, y: 2.0)))
                technique?.setValue(screenSize,
                                    forKeyPath: "screen_size")
                
                // I want to set nil texture at first. becaus of error...
//                technique?.setValue(nil, forKey: "volume")
//                technique?.setObject(nil, forKeyedSubscript: "volume" as NSCopying)
                
                self.technique = technique
                scnView.technique = technique
            }
        }
    }
}
